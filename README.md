## Conteúdo do Basecamp

* Fundamentos da programação
* HTML & CSS
* Lógica de programação
* Javascript
* ECMAScript 6+

# Projeto TodoList

## Rodando o Backend (servidor)

#### Vá para a pasta server
$ cd /ES6+/Projeto-Todolist/backend

#### Instale as dependências
$ npm install

#### Execute a aplicação em modo de desenvolvimento
$ npm run dev

## Rodando a aplicação web (Frontend)

#### Vá para a pasta da aplicação Frontend
$ cd /ES6+/Projeto-Todolist/frontend

#### Instale as dependências
$ npm install

#### Execute a aplicação em modo de desenvolvimento
$ npm run dev

#### Acesse http://localhost:3000
